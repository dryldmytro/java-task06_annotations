package com.dryl;

public class NewClass {
  private String name = "Dima";
  private int age = 23;
  public int burn = 7;

  public NewClass(String name) {
    this.name = name;
  }

  public NewClass() {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
