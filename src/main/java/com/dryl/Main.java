package com.dryl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
  public static final Logger logger = LogManager.getLogger(Main.class);

  public static void main(String[] args)
      throws NoSuchMethodException, NoSuchFieldException, IllegalAccessException {
    logger.info("hello");
    Main main = new Main();
    Class<?>c = main.getClass();
    Method m  = c.getMethod("methodWithAnnotation");
    MyAnnotations ma = m.getAnnotation(MyAnnotations.class);
    logger.trace(ma.timesToCall()+" "+ma.books()[0]+" "+ma.name());
    logger.trace(ma);
    Class<Integer> cl = Integer.TYPE;
    cl.getName();
    main.methodWithAnnotation();
  }

  @MyAnnotations(name = "Joe", books = {"Java","C++"})
  public void methodWithAnnotation() throws NoSuchFieldException, IllegalAccessException {
    NewClass nc = new NewClass();
    Class cl = nc.getClass();
    System.out.println(cl.getName());
    System.out.println(cl.getSimpleName());
    logger.info(Modifier.isPublic(cl.getModifiers()));
    logger.info(Modifier.isAbstract(cl.getModifiers()));
    logger.info(cl.getSuperclass().getName()+", "+cl.getPackage());
    Constructor[] constructor = cl.getConstructors();
    for (Constructor s:constructor ) {
      System.out.println(s);
    }
    Method[] method = cl.getDeclaredMethods();
    for (Method m: method) {
      logger.info(m);
    }
    Field f = NewClass.class.getDeclaredField("age");
    f.setAccessible(true);
    System.out.println(f.get(nc));
    f.set(nc,20);
    System.out.println(f.get(nc));

  }

}
